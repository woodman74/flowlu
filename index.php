<?php

  ini_set('error_reporting', E_ALL);
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);

  //Подключаемся к БД
  function connectDB() {
    $dbname = 'test';
    $login  = 'root';
    $pswd   = 'admin';
    try {
      $dbh = new PDO('mysql:dbname='.$dbname.';host=localhost', $login, $pswd);
    } catch (PDOException $e) {
      die($e->getMessage());
    } return $dbh;
  }

  $dbh = connectDB();
  $query = 'SELECT * FROM flowlu;';
  $sql = $dbh->prepare($query);
  $sql->execute();
  $array = $sql->fetchAll(PDO::FETCH_ASSOC);
?>

<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script src="js/init.js"></script>
  <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
</head>
<style>
.form {
  width: 400px;
  margin: auto;
  border: 1px solid #e1e1e1;
  padding: 20px;
  margin-top: 20px;
}

table {
  border: 1px solid #a1a1a1;
}

table td {
  padding: 4px 10px;
  border: 1px solid #a1a1a1;
}

</style>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="form">
          <p>Введите строку для проверки:</p>
          <input id="query">
          <a onclick="query();" class="btn btn-default btn-md">Отправить</a>
        </div>
      </div>

      <div class="col-md-12">
        <div class="form">
          <table>
            <tbody>
              <tr>
                <td>Запрос</td>
                <td>Результат</td>
              </tr>
                <?php
                  foreach ($array as $ar) {
                    echo "<tr>";
                    echo "<td>".$ar['query']."</td>";
                    echo "<td>".$ar['result']."</td>";
                    echo "</tr>";
                  }
                  #$json = json_encode($array);
                  #echo $json;
                ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
