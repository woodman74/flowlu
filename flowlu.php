<?php

//Подключаемся к БД
function connectDB() {
  $dbname = 'test';
  $login  = 'root';
  $pswd   = 'admin';
  try {
    $dbh = new PDO('mysql:dbname='.$dbname.';host=localhost', $login, $pswd);
  } catch (PDOException $e) {
    die($e->getMessage());
  } return $dbh;
}

function brackets($string) {

  $arr = str_split($string);

  $count  = 0;
  $open   = [ '<', '{', '[', '(' ]; # Варианты открытых скобок для проверки
  $close  = [ '>', '}', ']', ')' ]; # Варианты закрытых скобок для проверки
  $status = [ 0, 0, 0, 0 ]; # Количество вхождений по типам
  $result = true;


  foreach ($close as $cls) { # Проверяем, если первый символ закрывающая скобочка, сразу отдаём отрицательный результат
    if ( $arr[0] == $cls ) { $result = false; }
  }

  if ($result == true) {
    foreach ($arr as $s) { # Проходим по всем символам строки
      foreach ($open as $key => $o ) { # Проверяем соответствует ли текущий символ однму из видов скобочек
        if ($o == $s) {
          $count = $count + 1;
          $status[$key] = $status[$key] + 1; # Если есть открытая скобочка меняем статус
          $lastOpen[$count] = $key;
        }
      }

      foreach ($close as $key => $c ) {
        if ($c == $s) {
          $status[$key] = $status[$key] - 1; # Если есть закрытая скобочка меняем статус
          if ($lastOpen[$count] != $key) { $result = false; }
          $count = $count - 1;
        }
      }
    }

    foreach ($status as $stt) { # Проверяем все ли открытые скобки были закрыты
        if ($stt != 0) { $result = false; }
    }
  }

  $dbh = connectDB();
  $q = 'INSERT INTO flowlu (query, result) VALUES("'.$string.'", "'.strval($result).'");';
  $sql = $dbh->prepare($q);
  $sql->execute();

  $answer = array('succes' => $result);
  $json = json_encode($answer);
  echo $json;
}

$query = $_POST['strQuery'];
brackets($query);

?>


